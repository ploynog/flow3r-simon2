# python imports
import random
import time
import math
 
# flow3r imports
from st3m.application import Application, ApplicationContext
 
from ctx import Context
from st3m.input import InputController, InputState
 
from st3m.ui.view import BaseView
import st3m.run
import bl00mbox
 
# from st3m.ui.interactions import CapScrollController
import leds
import os

prev = None
class SimonAudio(BaseView):
    def __init__(self) -> None:
        super().__init__()
        self._blm = bl00mbox.Channel("Simon")

        # Sawtooth oscillator
        self._osc = self._blm.new(bl00mbox.patches.tinysynth)
        # Distortion plugin used as a LUT to convert sawtooth into custom square
        # wave.
        self._dist = self._blm.new(bl00mbox.plugins._distortion)
        # Lowpass.
        self._lp = self._blm.new(bl00mbox.plugins.lowpass)

        # Wire sawtooth -> distortion -> lowpass
        self._osc.signals.output = self._dist.signals.input
        self._dist.signals.output = self._lp.signals.input
        self._lp.signals.output = self._blm.mixer

        # Closest thing to a waveform number for 'saw'.
        self._osc.signals.waveform = 20000
        self._osc.signals.attack = 1
        self._osc.signals.decay = 0

        # Built custom square wave (low duty cycle)
        table_len = 129
        self._dist.table = [
            32767 if i > (0.1 * table_len) else -32768 for i in range(table_len)
        ]

        self._lp.signals.freq = 4000
        self._tone = 0
        self._playlen = 0
        self._t = 0
        self._tonedest = 0

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._t = self._t + delta_ms
        if self._t < self._playlen:
            tonedest = float(self._tonedest)
            self._tone = self._tone * 0.7 + tonedest * 0.3
            tone = self._tone if (abs(self._tone - tonedest)) > 0.1 else tonedest
            self._osc.signals.pitch.tone = tone
            self._osc.signals.trigger.start()
        else:
            self._osc.signals.trigger.stop()

    def do_play(self, pitch: int, duration: int):
        self._t = 0
        self._playlen = duration
        self._tonedest = pitch

class SimonVisual(BaseView):
    def __init__(self) -> None:
        self.showSegment = 0
        self.any_pressed = False
        self.SEGMENT_COLORS = [
            (231/255, 84/255, 26/255),
            (231/255, 84/255, 26/255),
            (34/255, 41/255, 169/255),
            (34/255, 41/255, 169/255),
            (246/255, 217/255, 39/255),
            (246/255, 217/255, 39/255),
            (240/255, 36/255, 43/255),
            (240/255, 36/255, 43/255),
            (35/255, 175/255, 80/255),
            (35/255, 175/255, 80/255)
        ]

    def draw(self, ctx: Context, do_qmark=False) -> None:
        leds.set_all_rgb(0, 0, 0)
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        if do_qmark:
            ctx.rgb(0.8, 0.8, 0.8)
            ctx.font = "Camp Font 2"
            ctx.font_size = 250
            ctx.move_to(-ctx.text_width("?") // 2, 50)
            ctx.text("?")
 
        if self.any_pressed:
            # Paint the background black
            ctx.rgb(*self.SEGMENT_COLORS[self.showSegment]).rectangle(-120, -120, 240, 240).fill()

            pi = 0.017
            ctx.save()
            ctx.rotate(pi * (60 + ((self.showSegment/2+1) * 360/5)))
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 120).fill()
            ctx.rotate(pi * (180-360/5))
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 120).fill()
            ctx.restore()
 
            for idx in range(5):
                led_pos = idx + -2 + self.showSegment*4
                if led_pos < 0:
                    led_pos = 40 + led_pos
                led_pos %= 40
                leds.set_rgb(led_pos, *self.SEGMENT_COLORS[self.showSegment])
 
        leds.update()
        

class CountIn(BaseView):
    def __init__(self, audio: SimonAudio):
        super().__init__()
        self.audio = audio
        self._vm = None
        self._progress = -1
        self._durations = [500, 250, 500, 250, 1000, 250]
        self._done = False
        self._curdur = 0

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        self._vm = vm
        self._progress = -1
        self._done = False
        self._curdur = 0


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.audio.think(ins, delta_ms)
        self._curdur = self._curdur + delta_ms

        if self._progress >= len(self._durations):
            self._vm.push(state.play_sequence)  
            return    

        if (self._progress == -1 or self._curdur > self._durations[self._progress]):
            self._progress += 1
            self._curdur = 0

            if self._progress >= len(self._durations):
                self._vm.push(state.play_sequence)  
                return      

            if self._progress in [0, 2]:
                self.audio.do_play(0, self._durations[self._progress])
            elif self._progress in [4]:
                self.audio.do_play(7, self._durations[self._progress])

    def draw(self, ctx: Context) -> None:
        leds.set_all_rgb(0, 0, 0)
        leds.update()        
        if self._progress in [0, 2]:
            ctx.rgb(255, 0, 0).rectangle(-120, -120, 240, 240).fill()
        elif self._progress in [1, 3, 5]:
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        elif self._progress in [4]:
            ctx.rgb(0, 255, 0).rectangle(-120, -120, 240, 240).fill()

        if self._progress in [0, 2, 4]:
            count = 3 - self._progress//2
            ctx.rgb(0.8, 0.8, 0.8)
            ctx.font = "Camp Font 2"
            ctx.font_size = 250
            ctx.move_to(-ctx.text_width(str(count)) // 2, 50)
            ctx.text(str(count))

class WonRound(BaseView):
    def __init__(self, audio: SimonAudio):
        super().__init__()
        self.audio = audio
        self._vm = None
        self._progress = -1
        self._durations = [150, 150, 150, 150, 350]
        self._done = False
        self._curdur = 0

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        self._vm = vm
        self._progress = -1
        self._done = False
        self._curdur = 0


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.audio.think(ins, delta_ms)
        self._curdur = self._curdur + delta_ms

        if self._progress >= len(self._durations):
            global state
            self._vm.push(state.play_sequence)
            return

        if (self._progress == -1 or self._curdur > self._durations[self._progress]):
            self._progress += 1
            self._curdur = 0

            if self._progress >= len(self._durations):
                return

            pitch_lu = [0, 4, 7, 9, 12]
            pitch = pitch_lu[self._progress]
            self.audio.do_play(pitch, self._durations[self._progress])

    def draw(self, ctx: Context) -> None:
        leds.set_all_rgb(0, 255, 0)
        leds.update()

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(0, 255, 0).arc(0, 0, 80, 0, 2*math.pi, True).fill()
        ctx.rgb(0, 0, 0).arc(0, 0, 40, 0, 2*math.pi, True).fill()

class LostRound(BaseView):
    def __init__(self, audio: SimonAudio):
        super().__init__()
        self.audio = audio
        self._vm = None
        self._progress = -1
        self._durations = [400, 400, 400, 400, 1000]
        self._done = False
        self._curdur = 0

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        self._vm = vm
        self._progress = -1
        self._done = False
        self._curdur = 0


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.audio.think(ins, delta_ms)
        self._curdur = self._curdur + delta_ms

        if self._progress >= len(self._durations):
            global state
            state.streak = [0]

            is_any = False
            for i in range(10):
                if self.input.captouch.petals[i].whole.down or self.input.captouch.petals[i].whole.pressed:
                    is_any = True

            if is_any:
                self._vm.push(state.count_in)
            return

        if (self._progress == -1 or self._curdur > self._durations[self._progress]):
            self._progress += 1
            self._curdur = 0

            state.current_score = len(state.streak) - 1
            if state.current_score > state.highscore:
                state.highscore = state.current_score
                state.save()

            if self._progress >= len(self._durations):
                return

            pitch_lu = [7, 3, 2, 1, 0]
            pitch = pitch_lu[self._progress]
            self.audio.do_play(pitch, self._durations[self._progress])

    def center_text(self, ctx: Context, text, y):
        ctx.move_to(-ctx.text_width(text)//2, y)
        ctx.text(text)

    def draw(self, ctx: Context) -> None:
        leds.set_all_rgb(255, 0, 0)
        leds.update()

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(255, 0, 0).arc(0, 0, 80, 0, 2*math.pi, True).fill()
        ctx.rgb(0, 0, 0).arc(0, 0, 40, 0, 2*math.pi, True).fill()

        ctx.rgb(255, 255, 255)
        ctx.font = "Camp Font 2"

        ctx.font_size = 30
        self.center_text(ctx, "score", -65)
        ctx.font_size = 100
        self.center_text(ctx, str(state.current_score), -10)

        ctx.font_size = 30
        self.center_text(ctx, "highscore", 20)
        ctx.font_size = 100
        self.center_text(ctx, str(state.highscore), 75)


class PlaySequence(BaseView):
    ON_TIME = 350
    OFF_TIME = 150

    def __init__(self, audio, visual) -> None:
        super().__init__()
        self.audio = audio
        self.visual = visual
        self._progress = -1
        self._tonemap = [0, 4, 7, 9, 12]
        self._vm = None
        self._curdur = 0
 
    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        self._vm = vm
        self._progress = -1
        self._done = False
        self._curdur = 0
        self.visual.any_pressed = False

    def draw(self, ctx: Context) -> None:
        self.visual.draw(ctx)
 
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        global state

        self._curdur += delta_ms
        self.visual.any_pressed = (self._curdur < self.ON_TIME and self._progress >= 0)

        if self._progress == -1 or self._curdur > (self.ON_TIME + self.OFF_TIME):
            self._progress += 1
            self._curdur = 0

            if self._progress >= len(state.streak):
                # Move over to Test phase
                self._vm.push(state.test_sequence)
                return
            i = state.streak[self._progress]

            self.audio.do_play(self._tonemap[i], self.ON_TIME)
            self.visual.showSegment = 2*i

        self.audio.think(ins, delta_ms)


class TestSequence(BaseView):
    def __init__(self, audio, visual) -> None:
        super().__init__()
        self.audio = audio
        self.visual = visual
        self._progress = 0
        self._tonemap = [0, 4, 7, 9, 12]
        self._input_sequence = []
        self._checked = False
        self._finished = False
 
    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        self._vm = vm
        self._progress = 0
        self.visual.any_pressed = False
        self._finished = False
        self._input_sequence = []
 
    def draw(self, ctx: Context) -> None:
        self.visual.draw(ctx, do_qmark=True)
 
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        is_any = False
        for i in range(5):
            if self.input.captouch.petals[2*i].whole.down:
                is_any = True
            if self.input.captouch.petals[2*i].whole.pressed:
                self._check_input()
                self._input_sequence += [i]
                self._checked = False
                self.audio.do_play(self._tonemap[i], 100)
                is_any = True
                self.visual.showSegment = 2*i
            if not is_any:
                self._check_input()
 
        self.visual.any_pressed = is_any
        self.audio.think(ins, delta_ms)

    def _check_input(self):
        if self._checked:
            return

        global state
        if self._input_sequence == state.streak[:len(self._input_sequence)]:
            if len(self._input_sequence) == len(state.streak):
                self._vm.push(state.won_round)
                if not self._finished:
                    # Sometimes this shit would be called twice
                    state.streak = state.streak + [random.randrange(5)]                
                self._finshed = True
        else:
            self._vm.push(state.lost_round)
        
        self._checked = True


class SimonSays(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        global state
        state = SimonState()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        global state
        self.vm.push(state.count_in)

    def draw(self, ctx: Context) -> None:
        pass


class SimonState:
    def __init__(self) -> None:
        self.audio = SimonAudio()
        self.visual = SimonVisual()
        self.count_in = CountIn(self.audio)
        self.lost_round = LostRound(self.audio)
        self.won_round = WonRound(self.audio)
        self.play_sequence = PlaySequence(self.audio, self.visual)
        self.test_sequence = TestSequence(self.audio, self.visual)
        self.streak = [0]
        self.current_score = 0
        self.highscore = 0

        self.path = '/flash/simon2_highscore'
        try:
            with open(self.path) as f:
                self.highscore = int(f.read())
        except OSError:
            self.highscore = 0

        except ValueError:
            self.highscore = 0

    def save(self):
        try:
            with open(self.path, "w+") as f:
                f.write(str(self.highscore))
                f.close()
        except:
            print("Failed to save highscore :(")

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    print("test")
    st3m.run.run_view(SimonSays(ApplicationContext()))
